from django.urls import path
from .views import home, PostList, PostDetail


urlpatterns = [
    path("", home, name="home"),
    path('posts/', PostList.as_view()),
    path('posts/<int:pk>/', PostDetail.as_view()),
]
