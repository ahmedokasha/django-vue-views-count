from django.db import models


class AnonymousVisitor(models.Model):
    visitor_ip = models.GenericIPAddressField()

    def __str__(self):
        return self.visitor_ip


class Post(models.Model):
    title = models.CharField(max_length=100)
    body = models.TextField()
    visited = models.ManyToManyField(AnonymousVisitor, blank=True)

    def __str__(self) -> str:
        return self.title

    def visitors_count(self):
        return self.visited.all().count()
