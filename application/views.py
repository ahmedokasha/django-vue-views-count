from django.http import Http404
from django.shortcuts import render
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Post, AnonymousVisitor
from .serializers import PostSerializer
from .utils import visitor_ip_address


def home(request):
    return render(request, template_name="home.html")


class PostList(APIView):
    """
    List all Post, or create a new Post.
    """

    def get(self, request, format=None):
        posts = Post.objects.all()
        serializer = PostSerializer(posts, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = PostSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PostDetail(APIView):
    """
    Retrieve, update or delete a Post instance.
    """

    def get_object(self, pk):
        try:
            return Post.objects.get(pk=pk)
        except Post.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        post = self.get_object(pk)
        serializer = PostSerializer(post)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        post = self.get_object(pk)
        serializer = PostSerializer(post, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        post = self.get_object(pk)
        post.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def patch(self, request, pk, format=None):
        post = self.get_object(pk)

        if not post.visited.filter(visitor_ip__exact=visitor_ip_address(request)):
            visitor_instance = AnonymousVisitor(visitor_ip=visitor_ip_address(request))
            visitor_instance.save()
            post.visited.add(visitor_instance)
            post.save()

        serializer = PostSerializer(post)
        return Response(serializer.data)


def record_visit():
    """
    1- save AnonymousVisitor using the function visitor_ip_address(request)
        visitor = AnonymousVisitor(visitor_ip=visitor_ip_address(request))
    2- Check if AnonymousVisitor view this post before :
        post = Post.Objects.filter(id=id)
        if ! visitor in post.visited:
            Visit.Objects.create(post, visitor, True)
    """
    pass


class Visits(APIView):
    def post(self, request, format=None):
        serializer = PostSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
