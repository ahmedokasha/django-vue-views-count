from django.contrib import admin
from .models import AnonymousVisitor, Post

admin.site.register(AnonymousVisitor)
admin.site.register(Post)
# admin.site.register(Visit)
